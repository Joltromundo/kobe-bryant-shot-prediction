import streamlit as st
from PreparacaoDados.pipeline import eda
from Treinamento.pipeline import treinamento_modelo
from Avaliacao.pipeline import avaliacao_modelo
from Monitoramento.pipeline import monitoramento
from Operacionalizador.pipeline import producao

st.set_option('deprecation.showPyplotGlobalUse', False)

def main():
    st.title("AutoML")
    tabs = ["Processamento dos Dados", "Treinamento de Modelo", "Avaliando Modelo", "Monitoramento", "Aplicando o Modelo"]
    selected = st.sidebar.radio("Menu:", tabs)
    select_tab(selected)
       

def select_tab(selected):
    if selected == "Processamento dos Dados":
        with st.spinner('Carregando...'):
            eda()
    elif selected == "Treinamento de Modelo":
        with st.spinner('Carregando...'):
            treinamento_modelo()
    elif selected == "Avaliando Modelo":
        with st.spinner('Carregando...'):
            avaliacao_modelo()
    elif selected == "Monitoramento":
        with st.spinner('Carregando...'):
            monitoramento()
    elif selected == "Aplicando o Modelo":
        with st.spinner('Carregando...'):
            producao()
    else:
        return "Opção inválida"

if __name__ == "__main__":
    main()