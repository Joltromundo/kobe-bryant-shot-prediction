def avaliacao_modelo():
    import Utils.methods as methods_utils
    import Utils.functions as func_utils
    import pandas as pd
    import streamlit as st
    
    st.sidebar.title("Avaliação do Modelo")    

    # Carregando o dataset de validação através do file uploader da barra lateral
    file_to_load  = st.sidebar.file_uploader("Carregar dataset de validação:", type=["csv", "parquet"])
    if file_to_load is not None:
        # Carregando o DataFrame de validação usando a função carregar_df do módulo Utils.functions
        dados_validacao = func_utils.carregar_df(file_to_load)                  
    else:
        st.empty()

    # Tentando recuperar os valores de algumas variáveis de sessão
    try:
        target_col = st.session_state.target_col    
        mlflow_db = st.session_state.mlflow_db
        experiment_id = st.session_state.experiment_id
        experiment_name = st.session_state.experiment_name    
        model_uri = st.session_state.mlflow_model_uri 
    except:
        target_col, mlflow_db, experiment_id, experiment_name, model_uri = "", "", "", "", "",  
    
    # Tentando selecionar a coluna alvo do dataset de validação
    try:
        target_col = st.sidebar.selectbox("Nome da coluna alvo:", options=dados_validacao.columns.tolist())
    except:
        target_col = ""
        
    # Recebendo ID do experimento, nome do experimento e o BD do MLflow da barra lateral
    experiment_id = st.sidebar.text_input("ID do experimento:", value=experiment_id )
    experiment_name = st.sidebar.text_input("Nome do experimento:", value=experiment_name)    
    mlflow_db = st.sidebar.text_input("BD MLflow:", value=mlflow_db)    
    
    # Selecionando entre o último run e o modelo registrado
    uri_choice = st.sidebar.selectbox("Escolha Último run ou Modelo registrado:", ["Último run", "Modelo registrado"])

    if uri_choice == "Último run":
        uri_formatado = func_utils.formatar_run_uri(model_uri)
    else:
        uri_formatado = func_utils.formatar_modelo_uri()
        
    # Recebendo o caminho do run/modelo da barra lateral
    modelo_logado = st.sidebar.text_input("Caminho do run/model:", value=uri_formatado)
    model_uri = modelo_logado

    # Tentando avaliar o modelo quando o botão "Avaliar Modelo" é clicado e os dados de validação não são nulos
    try:
        if st.sidebar.button("Avaliar Modelo") and dados_validacao is not None:
            # Chamando a função avaliar_modelo para realizar a avaliação
            resultado, acuracia, relatorio_classificacao_df, tabela_confusao, roc_auc, precisao, recall, f1, mcc = avaliar_modelo(experiment_id, experiment_name, modelo_logado, dados_validacao, target_col, mlflow_db)
    
            # Dividindo a interface em duas colunas
            col1, col2 = st.columns(2)
            # Obtendo a lista de métricas de avaliação do módulo Utils.methods
            lista_metricas = list(methods_utils.select_metrics_ev.items())
            
            # Mostrando as métricas de avaliação na primeira coluna
            with col1:
                for metrica_pt, metrica_en in lista_metricas[:4]:
                    st.write(f"{metrica_pt}: {round(resultado.metrics[metrica_en], 4)}")
    
            # Mostrando as métricas de avaliação na segunda coluna
            with col2:       
                for metrica_pt, metrica_en in lista_metricas[-4:]:
                    st.write(f"{metrica_pt}: {round(resultado.metrics[metrica_en], 4)}")
    
            # Mostrando o relatório de classificação
            st.write("Classification Report:")
            st.write(relatorio_classificacao_df) 
    
            # Mostrando a matriz de confusão
            st.write("Matrix de Confusão")
            st.table(tabela_confusao)
            
            # Mostrando as métricas
            st.write("Métricas")
            st.write(resultado.metrics)
    except Exception as e:
        # Lidando com exceções
        st.write(f"Verificar parâmetros. {e}")

def avaliar_modelo(experiment_id, experiment_name, modelo_logado, dados_validacao, target_col, mlflow_db):
    import pandas as pd
    import mlflow
    import pycaret.classification as pc
    import Utils.functions as func_utils
    from sklearn import metrics
    from mlflow.models.signature import infer_signature
    from sklearn.metrics import accuracy_score, classification_report, confusion_matrix, roc_curve, roc_auc_score, precision_recall_curve, f1_score, matthews_corrcoef
    
    # Configurando o URI de rastreamento do MLflow
    mlflow.set_tracking_uri(f"http://127.0.0.1:5000")
    #mlflow.set_tracking_uri(f"sqlite:///Code/Models/{mlflow_db}.db") 

    # Obtendo o ID do experimento
    experiment_id = func_utils.get_experiment_id(experiment_id)

    # Iniciando o run do MLflow e definindo o nome do run
    with mlflow.start_run(experiment_id=experiment_id, run_name=experiment_name) as run:
    
        # Carregando o modelo treinado
        model = mlflow.sklearn.load_model(modelo_logado)
    
        # Extraindo features do dataset de validação
        X_validacao = dados_validacao.drop(columns=[target_col])
    
        # Configurando o ambiente PyCaret
        pc.setup(data = dados_validacao)
        # Fazendo previsões
        predictions = pc.predict_model(model, raw_score=True)
        predictions.drop('prediction_score_0', axis=1, inplace=True)
        predictions.rename({'prediction_score_1': 'prediction_score'}, axis=1, inplace=True)

        # Calculando métricas de avaliação
        precisao = metrics.precision_score(predictions[target_col], predictions['prediction_label'])
        recall = metrics.recall_score(predictions[target_col], predictions['prediction_label'])
        acuracia = metrics.accuracy_score(predictions[target_col], predictions['prediction_label'])
        f1 = metrics.f1_score(predictions[target_col], predictions['prediction_label'])
        mcc = metrics.matthews_corrcoef(predictions[target_col], predictions['prediction_label'])
        roc_auc = metrics.roc_auc_score(predictions[target_col], predictions['prediction_label'])
        relatorio_classificacao = metrics.classification_report(predictions[target_col], predictions['prediction_label'], output_dict=True)        
        relatorio_classificacao_df = pd.DataFrame(relatorio_classificacao)
        tabela_confusao = metrics.confusion_matrix(predictions[target_col], predictions['prediction_label'])
        
        # Criando uma assinatura do modelo
        signature = infer_signature(X_validacao, predictions)

        # Registrando o modelo no MLflow
        mlflow.sklearn.log_model(model, "pycaret-model", signature=signature)
    
        model_uri = mlflow.get_artifact_uri("pycaret-model")
        eval_data = dados_validacao 
        # Avaliando o modelo no MLflow
        resultado = mlflow.evaluate(
            model_uri,
            eval_data,
            targets=target_col,
            model_type="classifier",
            evaluators=["default"],
        )
       
        # Criando uma tabela de confusão formatada
        tabela_confusao = pd.DataFrame({
        'Positivo': ['Verdadeiro Positivo (VP)', tabela_confusao[1, 1], 'Falso Positivo (FP)', tabela_confusao[0, 1]],
        'Negativo': ['Falso Negativo (FN)', tabela_confusao[1, 0], 'Verdadeiro Negativo (VN)', tabela_confusao[0, 0]]
        })
        tabela_confusao = tabela_confusao.style.set_table_styles([
            {'selector': 'th', 'props': 'display: none'},
            {'selector': 'td', 'props': 'padding: 0'}
        ])        

        # Salvando o relatório de classificação em formato Parquet
        relatorio_classificacao_df_path = "./Docs/Report_aval.parquet"        
        relatorio_classificacao_df.to_parquet(relatorio_classificacao_df_path, index=False)
        
        # Logando o relatório de classificação como um artefato no MLflow
        mlflow.log_artifact(relatorio_classificacao_df_path)
        
    # Finalizando o run do MLflow
    mlflow.end_run()
    # Retornando resultados da avaliação
    return resultado, acuracia, relatorio_classificacao_df, tabela_confusao, roc_auc, precisao, recall, f1, mcc  