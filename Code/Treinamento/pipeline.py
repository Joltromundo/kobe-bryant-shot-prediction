def treinamento_modelo():
    import streamlit as st
    import pandas as pd
    import os
    import time
    import Utils.methods as metodos_utils
    import Utils.functions as func_utils

    # Configurações da barra lateral
    st.sidebar.title("Treinamento do Modelo")
    st.sidebar.subheader("Configurações")

    # Carregar estatísticas da sessão
    try:
        col_alvo = st.session_state.col_alvo    
        mlflow_db = st.session_state.mlflow_db
        id_experimento = st.session_state.id_experimento
    except:
        col_alvo, mlflow_db, id_experimento = "", "", ""
    
    arquivo_carregado = st.sidebar.file_uploader("Carregar Conjunto de Dados de Treino:", type=["csv", "parquet"])
    if arquivo_carregado is not None:
        df_carregado = func_utils.carregar_df(arquivo_carregado)
            
        # Obter nomes das colunas e armazená-los no estado da sessão
        colunas = df_carregado.columns.tolist()
        st.session_state.colunas_df = colunas
        st.session_state.df_carregado = df_carregado
        
    tamanho_teste_percentual = st.sidebar.number_input("Tamanho da base de teste(%): ", min_value=5, max_value=100, value=20, step=5)
    tamanho_teste = tamanho_teste_percentual / 100.0

    # Popular colunas a serem mantidas e coluna alvo após carregar os nomes das colunas
    if 'colunas_df' in st.session_state:
        colunas_a_manter = st.sidebar.multiselect("Colunas a Analisar(Alvo incluso):", options=st.session_state.colunas_df, default=st.session_state.colunas_df)
        col_alvo = st.sidebar.selectbox("Coluna alvo:", options=st.session_state.colunas_df)

    # Seleção de modelos com Popover
    st.sidebar.subheader("Modelos")
    modelos_selecionados = st.sidebar.multiselect("Escolha os modelos:", options=list(metodos_utils.select_models_class.keys()), format_func=lambda x: x)
    modelos_selecionados = [metodos_utils.select_models_class[model] for model in modelos_selecionados]
    modelos = modelos_selecionados    
    
    # Parâmetros 
    st.sidebar.subheader("Parâmetros(Dados)")
    # Outliers
    remover_outliers = st.sidebar.toggle("Remover 'Outliers'", value=False)  
    if remover_outliers:
        metodo_outliers_selecionado = st.sidebar.selectbox("Método de Remoção de 'Outliers':", list(metodos_utils.select_outliers_methods.keys()), index=0)
        metodo_outliers = metodos_utils.select_outliers_methods[metodo_outliers_selecionado]
    else:
        metodo_outliers = "iforest"
    # Desbalanceamento
    balancear_desbalanceamento = st.sidebar.toggle("Balancear(SMOTE) ", value=False)
    # Normalizar
    normalizar = st.sidebar.toggle("Normalizar", value=False)    
    if normalizar:
        metodo_normalizacao_selecionado = st.sidebar.selectbox("Método de normalização:", list(metodos_utils.select_normalize_methods.keys()), index=0)
        metodo_normalizacao = metodos_utils.select_normalize_methods[metodo_normalizacao_selecionado]
    else:
        metodo_normalizacao = "zscore"    
    #
    st.sidebar.subheader("Parâmetros(Setup)")
    folds = st.sidebar.number_input("Nº Folds: ", min_value=1, max_value=50, value=10)
    estrategia_folds_selecionada = st.sidebar.selectbox("Estratégia de dobras(folds):", list(metodos_utils.select_fold.keys()), index=0)
    estrategia_folds = metodos_utils.select_fold[estrategia_folds_selecionada]    
    #
    st.sidebar.subheader("Parâmetros(Tuning)")
    otimizador_selecionado = st.sidebar.selectbox("Otimizador:", options=list(metodos_utils.select_optimize.keys()), format_func=lambda x: x)
    otimizador = metodos_utils.select_optimize[otimizador_selecionado]   
    n_iteracoes = st.sidebar.number_input("Nº Iterações: ", min_value=1, max_value=50, value=10)
    # 
    st.sidebar.subheader("Experimento")
    mlflow_db = st.sidebar.text_input("BD rastreamento(MLflow):", f"{mlflow_db}")
    id_experimento = st.sidebar.text_input("ID do experimento:", f"{id_experimento}")
    nome_experimento = st.sidebar.text_input("Nome do experimento(run):", "Treinamento")

    # Plots de Classificação com Popover
    st.sidebar.subheader("Plots de Classificação")
    plots_selecionados = st.sidebar.multiselect("Plots a serem gerados(Artefatos):", options=list(metodos_utils.select_plots_class.keys()), format_func=lambda x: x)
    plots_selecionados = [metodos_utils.select_plots_class[plot] for plot in plots_selecionados]
    plots_classificacao = plots_selecionados

    # Carregar variáveis da sessão
    try:
        st.session_state.col_alvo = col_alvo
    except:
        st.write("")
    st.session_state.mlflow_db = mlflow_db
    st.session_state.id_experimento = id_experimento
    st.session_state.nome_experimento = nome_experimento    
        
    # Botão de Treinamento
    try:
        if st.sidebar.button("Treinar"):
            # Carregar dataset da sessão
            df_limpo = st.session_state.df_carregado
            df_limpo = df_limpo[colunas_a_manter]
    
            # Parâmetros de Execução
            parametros = {"seed": 10, "nome_experimento": nome_experimento}
    
            # Chamada de Treinamento
            resultado, perda_treino, perda_teste, f1_treino, f1_teste, tamanho_treino_base, tamanho_teste_base, leaderboard = treinar_modelo(id_experimento, nome_experimento, tamanho_teste, modelos, parametros, df_limpo, col_alvo, mlflow_db, plots_classificacao, otimizador, n_iteracoes, normalizar, metodo_normalizacao, balancear_desbalanceamento, remover_outliers, metodo_outliers, folds, estrategia_folds)
    
            col1, col2 = st.columns(2)
            with col1:
                # Iterar sobre o dicionário de métricas e imprimir os resultados
                for metrica_pt, metrica_en in metodos_utils.select_metrics_class.items():
                    st.write(f"{metrica_pt}: {round(resultado.metrics[metrica_en],4)}")
                st.write(f"Tamanho Dataset Treino: {tamanho_treino_base}")
                st.write(f"Otimizador: {otimizador}")
    
            with col2:                    
                st.write(f"Perda Log Treino: {round(perda_treino,4)}")
                st.write(f"Perda Log Teste: {round(perda_teste,4)}")
                st.write(f"F1-Score Treino: {round(f1_treino,4)}")
                st.write(f"F1-Score Teste: {round(f1_teste,4)}")
                st.write(f"Tamanho Dataset Teste: {tamanho_teste_base}")
                st.write(f"Nº Iterações: {n_iteracoes}")
                     
            st.write("Métricas dos Modelos Comparados")
            st.write(leaderboard)
    except Exception as e:
        st.write(f"Verificar entrada dos parâmetros.")

    # Exibir plots criados na última execução
    arquivos_plot = [f for f in os.listdir() if f.endswith(('.png', '.jpg', '.jpeg')) and time.time() - os.path.getmtime(f) < 60]
    for arquivo_plot in arquivos_plot:
        st.image(arquivo_plot, caption=arquivo_plot, use_column_width=True)

def treinar_modelo(id_experimento, nome_experimento, tamanho_teste, modelos, parametros, dataset_limpo, col_alvo, mlflow_db, plots_classificacao, otimizador, n_iteracoes, normalizar, metodo_normalizacao, balancear_desbalanceamento, remover_outliers, metodo_outliers, folds, estrategia_folds) -> None:
    from sklearn.metrics import log_loss, f1_score
    from sklearn.model_selection import train_test_split
    from sklearn import metrics
    from pycaret.classification import get_leaderboard
    from mlflow.models.signature import infer_signature
    from mlflow.tracking import MlflowClient
    import pycaret.classification as pc
    import mlflow
    import numpy as np
    import pandas as pd
    import streamlit as st
    import Utils.functions as func_utils
    
    # Configurar Rastreamento do MLflow
    mlflow.set_tracking_uri(f"http://127.0.0.1:5000")
    #mlflow.set_tracking_uri(f"sqlite:///Code/Models/{mlflow_db}.db")   

    # Definir Nome do Experimento    
    id_experimento = func_utils.get_experiment_id(id_experimento)

    #Divisão Treino, Teste
    treino_base, teste_base = train_test_split(dataset_limpo, test_size=tamanho_teste) 

    # Salvando Conjunto de Treino
    caminho_treino_base = "./Data/Processed/base_train.parquet"
    treino_base.to_parquet(caminho_treino_base, index=False)
    
    # Salvando Conjunto de Teste
    caminho_teste_base = "./Data/Processed/base_test.parquet"
    teste_base.to_parquet(caminho_teste_base, index=False)    
    
    # Iniciar Execução do MLflow e Configurar Modelo
    with mlflow.start_run(experiment_id=id_experimento, run_name=nome_experimento) as run:
        experimento = pc.setup(
            session_id=10,
            data=treino_base,
            test_data=teste_base,
            target=col_alvo,
            profile=False,
            fold=folds,
            fold_strategy=estrategia_folds,
            fix_imbalance=balancear_desbalanceamento,
            remove_outliers=remover_outliers,
            outliers_method=metodo_outliers,
            normalize=normalizar,
            normalize_method = metodo_normalizacao,
            remove_multicollinearity=True,
            multicollinearity_threshold=0.95,
            experiment_name=nome_experimento,
            log_experiment=False,
        )

        # Treinamento e Sintonia do Modelo
        print("***** Comparando Modelos...")
        modelo = pc.compare_models(n_select=1, sort="Accuracy", include=modelos)
        print('("***** Selected Model: ', modelo)

        print("***** Sintonizando Modelo...")
        modelo_ajustado = pc.tune_model(modelo, optimize=otimizador, search_library="scikit-learn", search_algorithm="random", n_iter=n_iteracoes, choose_better=True,)

        print("***** Calibrando Modelo...")
        modelo_calibrado = pc.calibrate_model(modelo_ajustado, method="sigmoid", calibrate_fold=5)
        # Melhor Modelo
        modelo_final = pc.finalize_model(modelo_calibrado)
        # Leaderboard
        leaderboard = get_leaderboard()
        
        # Salvando Modelo  
        print("***** Salvando Modelo...")
        try:
            pc.save_model(modelo_final, nome_experimento)
            mlflow.log_artifact(f"./Code/Models/TrainedModels/{nome_experimento}.pkl") 
        except Exception as e:
            print(f"Failed to save model. Error: {e}")

        # Carregar Pipeline Completo
        pipeline_modelo = pc.load_model(f'./{nome_experimento}')
        
        # Assinatura do Modelo Inferida pelo MLflow
        features_modelo = list(dataset_limpo.drop(col_alvo, axis=1).columns)
        inf_signature = infer_signature(dataset_limpo[features_modelo], pipeline_modelo.predict(dataset_limpo.drop(col_alvo, axis=1)))
        
        # Exemplo de entrada para MLmodel
        n_exemplos = 6
        exemplo_input = {x: dataset_limpo[x].values[:n_exemplos] for x in features_modelo}
        
        # Log do pipeline de modelagem do sklearn e registrar como uma nova versao
        mlflow.sklearn.log_model(
            sk_model=pipeline_modelo,
            artifact_path="pycaret-model",
            registered_model_name=nome_experimento,
            signature = inf_signature,
            input_example = exemplo_input
        )

        # Avaliar o modelo no MLflow
        uri_modelo = mlflow.get_artifact_uri("pycaret-model")
        dados_avaliacao = dataset_limpo  
        resultado = mlflow.evaluate(
            uri_modelo,
            dados_avaliacao,
            targets=col_alvo,
            model_type="classifier",
            evaluators=["default"],
        )

        # Obter o URI do modelo MLflow e armazená-lo na sessão
        st.session_state.uri_modelo_mlflow = uri_modelo        
                
        # Calcular perda log e F1-score
        pred_treino, pred_teste = modelo_final.predict(treino_base.drop(col_alvo, axis=1)), modelo_final.predict(teste_base.drop(col_alvo, axis=1))
        perda_treino, perda_teste = log_loss(treino_base[col_alvo], pred_treino), log_loss(teste_base[col_alvo], pred_teste)
        f1_treino, f1_teste = f1_score(treino_base[col_alvo], pred_treino), f1_score(teste_base[col_alvo], pred_teste)
        tamanho_treino_base, tamanho_teste_base = treino_base.shape[0], teste_base.shape[0]
    
        # Log de Métricas
        mlflow.log_metric("Perda Log Treino", perda_treino)
        mlflow.log_metric("Perda Log Teste", perda_teste)
        mlflow.log_metric("F1-score Treino", f1_treino)
        mlflow.log_metric("F1-score Teste", f1_teste)
        mlflow.log_metric("Tamanho Conjunto Treino", tamanho_treino_base)
        mlflow.log_metric("Tamanho Conjunto Teste", tamanho_teste_base)
        
        # Log de Parâmetros do Experimento
        mlflow.log_params(parametros)
        mlflow.log_param("Tamanho Teste", tamanho_teste) 
        
        # Log de Artefatos  
        mlflow.log_artifact(caminho_treino_base)
        mlflow.log_artifact(caminho_teste_base)        
        
        for tipo_plot in plots_classificacao:
            print(f"Salvando Artefato Plot: {tipo_plot}")
            try:
                artefato = pc.plot_model(modelo_final, plot=tipo_plot, save=True)
                mlflow.log_artifact(artefato)
            except Exception as e:
                print(f"Failed to save artifact plot for: {tipo_plot}. Error: {e}")
                
        print("**** FIM ****")
    mlflow.end_run()
    return resultado, perda_treino, perda_teste, f1_treino, f1_teste, tamanho_treino_base, tamanho_teste_base, leaderboard            
