def eda():
    import streamlit as st
    import pandas as pd
    import mlflow
    import Utils.plot as plot_utils
    import Utils.methods as method_utils
    import Utils.functions as func_utils

    # Configuração para evitar avisos de depreciação
    st.set_option('deprecation.showPyplotGlobalUse', False)
    st.sidebar.title("Análise Exploratória dos Dados")

    # Carregar o conjunto de dados
    st.sidebar.subheader("Configurações")
    arquivo_carregado = st.sidebar.file_uploader("Carregar arquivo de dados brutos:", type=["csv", "parquet"])
    if arquivo_carregado is not None:
        df = func_utils.carregar_df(arquivo_carregado)

        # Visualizar os dados brutos 
        st.subheader("Exibição dos Dados Originais")
        st.write(df)
        
        # Seleção de colunas para análise
        colunas_selecionadas = st.sidebar.multiselect("Selecione as colunas para filtrar:", options=sorted(df.columns.tolist()), default=sorted(df.columns.tolist()))
        coluna_alvo = st.sidebar.selectbox("Variável alvo:", options=colunas_selecionadas)

        st.sidebar.subheader("Parâmetros")
        # Realizar a limpeza de dados
        if st.sidebar.toggle("Remover valores faltantes.", value=True):
            df_limpo = df[colunas_selecionadas].dropna()
        else:
            df_limpo = df[colunas_selecionadas]

        # Balanceamento
        balanceamento = st.sidebar.toggle("Balanceamento (SMOTE)", value=True)
        
        # Remoção de outliers
        remover_outliers = st.sidebar.toggle("Remoção de outliers", value=True)  
        if remover_outliers:
            metodo_outliers_selecionado = st.sidebar.selectbox("Método de remoção de outliers:", list(method_utils.select_outliers_methods.keys()), index=0)
            metodo_outliers = method_utils.select_outliers_methods[metodo_outliers_selecionado]
        else:
            metodo_outliers = "iforest"
        
        # Normalização
        normalizar = st.sidebar.toggle("Normalização", value=True)    
        if normalizar:
            metodo_normalizacao_selecionado = st.sidebar.selectbox("Método de normalização:", list(method_utils.select_normalize_methods.keys()), index=0)
            metodo_normalizacao = method_utils.select_normalize_methods[metodo_normalizacao_selecionado]
        else:
            metodo_normalizacao = "zscore"         

        try:
            st.session_state.df_limpo = df_limpo
        except:
            df_limpo = df_limpo
        try:
            # Resumo Estatístico Descritivo
            st.subheader("Resumo Estatístico Descritivo")            
            st.write(df_limpo.describe())
        except Exception as e:
            st.write(f"Selecione as colunas desejadas para análise.")

        try:
            # Visualização de dados faltantes
            df_faltante = df_limpo.isnull().sum().reset_index()
            df_faltante = df_faltante.rename(columns={"index": "Colunas", 0: "N º N/A"})    
            st.subheader("Visualização de Valores Faltantes")
            col1, col2 = st.columns(2)
            with col1:
                st.write(df_faltante)                
            with col2:
                st.write(df_limpo.dtypes)
        except Exception as e:
            st.write(f"Selecione as colunas desejadas para análise.")
        
        st.sidebar.subheader("Gráfico de pré-processamento")
        st.header("Gráfico:")
        # Escolha do tipo de gráfico
        tipo_grafico = st.sidebar.selectbox("Escolher tipo de gráfico:", ['Heatmap', 'Relacional', 'Distribuição', 'Pairplot', 'Categorias'], index=None)
        
        # Chamada da função para gerar o plot
        try:
            plot_utils.plot_eda(df_limpo, colunas_selecionadas, tipo_grafico)
        except Exception as e:
            st.error(f"Erro: {e}")     

        st.sidebar.subheader("Parâmetros para rastreamento")
        id_experimento = st.sidebar.text_input("ID do experimento:", value="Mamba" )
        nome_experimento = st.sidebar.text_input("Nome experimento (run):", value="PreparacaoDados")
        banco_mlflow = st.sidebar.text_input("Banco rastreamento (MLflow):", "mlruns")

        try:
            st.session_state.banco_mlflow = banco_mlflow
            st.session_state.id_experimento = id_experimento
            st.session_state.nome_experimento = nome_experimento 
            st.session_state.coluna_alvo = coluna_alvo 
        except:
            st.write("")
        
        # Salvar o conjunto de dados pré-processado
        st.sidebar.subheader("Salvar Dados")        
        nome_arquivo = st.sidebar.text_input("Nome do Dataset Processado:", "dados_limpos")
        formato_salvar = st.sidebar.selectbox("Formato do arquivo:", ["Parquet", "CSV"])        
        try:
            if st.sidebar.button("Aplicar e Salvar"):
                try:
                    df_limpo = st.session_state.df_limpo
                except:
                    df_limpo = df_limpo
                # Chamada da função para pré-processamento
                df_limpo = processar_dados(df_limpo, remover_outliers, metodo_outliers, balanceamento, normalizar, metodo_normalizacao, banco_mlflow, id_experimento, nome_experimento, coluna_alvo, arquivo_carregado, nome_arquivo, formato_salvar)
        except Exception as e:
            st.write(f"Verifique os parâmetros de entrada: {e}")

def processar_dados(df_limpo, remover_outliers, metodo_outliers, balanceamento, normalizar, metodo_normalizacao, banco_mlflow, id_experimento, nome_experimento, coluna_alvo, arquivo_carregado, nome_arquivo, formato_salvar):
    import pycaret.classification as pc
    import pandas as pd
    import streamlit as st
    import mlflow
    import Utils.functions as func_utils

    # Verificar se o experimento existe, senão criar um novo
    id_experimento = func_utils.get_experiment_id(id_experimento)

    mlflow.set_tracking_uri(f"http://127.0.0.1:5000")
    #mlflow.set_tracking_uri(f"sqlite:///Code/Models/{banco_mlflow}.db")   

    with mlflow.start_run(experiment_id=id_experimento, run_name=nome_experimento):
        # Pré-processamento com PyCaret
        data_exp = pc.setup(df_limpo, 
                            target=coluna_alvo, 
                            remove_outliers=remover_outliers, 
                            outliers_method=metodo_outliers, 
                            fix_imbalance=balanceamento,
                            normalize=normalizar, 
                            normalize_method=metodo_normalizacao
                           )
        df_limpo = data_exp.get_config("dataset_transformed") 

        base_save_path = "./Data/Processed/"
        if formato_salvar == "CSV":
            save_path = base_save_path + nome_arquivo + ".csv"
            df_limpo = df_limpo.sort_index(axis=1)
            df_limpo.to_csv(save_path, index=False)            
        elif formato_salvar == "Parquet":
            save_path = base_save_path + nome_arquivo + ".parquet"
            df_limpo = df_limpo.sort_index(axis=1)
            df_limpo.to_parquet(save_path, index=False)
            
        st.sidebar.success("Dados salvos!")
        st.subheader("Visualização processada")
        st.write(f"Após a filtragem, o conjunto de dados possui {df_limpo.shape[0]} amostras, {df_limpo.shape[1]} atributos e {df_limpo.isna().sum().sum()} valores faltantes")
        st.write(df_limpo)        
        
        #  Logging
        mlflow.log_param("selected_features", df_limpo.columns.tolist())      
        mlflow.log_param("remove_outliers", remover_outliers)
        mlflow.log_param("outliers_method", metodo_outliers)
        mlflow.log_param("fix_imbalance", balanceamento)
        mlflow.log_param("normalize", normalizar)
        mlflow.log_param("normalize_method", metodo_normalizacao)
        mlflow.log_param("raw_dataset", arquivo_carregado.name)
        mlflow.log_param("processed_dataset", f"{nome_arquivo}.{formato_salvar.lower()}")
        mlflow.log_metric("processed_dataset_size", df_limpo.shape[0])
        mlflow.log_artifact(save_path)
        print("*** Dataset Processado Salvo com sucesso ***")
            
    mlflow.end_run()
    return df_limpo