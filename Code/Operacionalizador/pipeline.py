def producao():
    import pandas as pd
    import streamlit as st
    import Utils.methods as utils_metodos
    import Utils.functions as utils_func

    st.sidebar.title("Aplicação do Modelo")
    
    # Carregar o dataset de produção
    file_to_load = st.sidebar.file_uploader("Carregar Dataset Produção:", type=["csv", "parquet"])
    if file_to_load is not None:
        df_producao = utils_func.carregar_df(file_to_load)            
    else:
        df_producao = ""
    
    # Tentativa de obter as colunas do dataset de produção
    try:
        columns = df_producao.columns.tolist()
    except:
        columns = "" 
    
    # Opção para selecionar se o dataset de produção possui uma coluna alvo
    alvo_bool = st.sidebar.toggle("Dataset tem coluna alvo?", value=True)
    if alvo_bool is True:
        target_col = st.sidebar.selectbox("Coluna alvo:", options=columns)
    
    # Entrada do nome do banco de dados MLflow
    mlflow_db = st.sidebar.text_input("BD rastreamento(MLflow):", "mlruns")

    # Tentativa de obter o ID do experimento da sessão
    try:
        experiment_id = st.session_state.experiment_id
        experiment_id = st.sidebar.text_input("ID do experimento:", value=experiment_id )
    except:
        experiment_id = st.sidebar.text_input("ID do experimento:", value="" )

    # Tentativa de obter o nome do experimento da sessão
    try:        
        experiment_name = st.session_state.experiment_name    
        experiment_name = st.sidebar.text_input("Nome do experimento avaliação:", value=experiment_name)  
    except:
        experiment_name = st.sidebar.text_input("Nome do experimento avaliação:", value="")   
    
    # Tentativa de obter o URI do modelo MLflow da sessão
    try:
        model_uri = st.session_state.mlflow_model_uri 
        model_uri = utils_func.formatar_run_uri(model_uri)
        model_uri = st.sidebar.text_input("Caminho do Modelo:", model_uri)
    except:
        model_uri = st.sidebar.text_input("Caminho do Modelo:", "")
    
    try:
        # Verificar se o botão "Aplicar Modelo" foi pressionado
        if st.sidebar.button("Aplicar Modelo"):
            # Se o dataset de produção possui uma coluna alvo
            if alvo_bool is True:
                # Chamada da função para aplicar o modelo no dataset de produção com coluna alvo
                report, acuracia, precision, f1, recall, df_result = modelo_alvo(df_producao, columns, mlflow_db, target_col, model_uri, experiment_id, experiment_name)
                # Exibir as métricas de avaliação e o relatório de classificação
                st.write(f"Acurária: {acuracia:.4f}")
                st.write(f"Precisão: {precision:.4f}")
                st.write(f"F1-Score: {f1:.4f}")
                st.write(f"Recall: {recall:.4f}")
                st.write(report)
                st.write(df_result)                
            # Se o dataset de produção não possui uma coluna alvo
            else:
                # Chamada da função para aplicar o modelo no dataset de produção sem coluna alvo
                df_result = modelo_producao(df_producao, mlflow_db, model_uri, experiment_id, experiment_name) 
                # Exibir o resultado
                st.write(df_result)

    except Exception as e:
        # Exibir mensagem de erro em caso de exceção
        st.write(f"Verificar entrada da parâmetros.{e}")

def modelo_alvo(df_producao, columns, mlflow_db, target_col, model_uri, experiment_id, experiment_name):
    import pandas as pd
    import mlflow
    import streamlit as st
    from sklearn.metrics import accuracy_score, classification_report, precision_score, f1_score, recall_score
    import Utils.methods as utils_metodos
    import Utils.functions as utils_func
    import os
    
    # Definição do URI do banco de dados MLflow
    os.environ['MLFLOW_TRACKING_URI'] = f"sqlite:///Code/Models/{mlflow_db}.db"

    # Configuração do ID do experimento
    experiment_id = utils_func.get_experiment_id(experiment_id)

    with mlflow.start_run(experiment_id=experiment_id, run_name=experiment_name) as run:
    
        # Carregamento do modelo treinado
        model = mlflow.sklearn.load_model(model_uri)
        
        # Carregamento do conjunto de dados
        df = df_producao
        
        # Remoção da coluna alvo do conjunto de dados de produção
        df_producao = df.drop(columns=[target_col])
        
        # Previsão das classes
        classes = model.predict(df_producao)
        probabilities_producao = model.predict_proba(df_producao)[:, 1]  
    
        # Criação de um DataFrame com as previsões
        df_predicoes = pd.DataFrame({'Previsoes': classes, 'Probabilidade_Classe_1': probabilities_producao})
        df_result = pd.concat([df, df_predicoes], axis=1)
        
        # Cálculo da acurácia
        acuracia = accuracy_score(df[target_col], classes)
        precision = precision_score(df[target_col], classes)
        f1 = f1_score(df[target_col], classes)
        recall = recall_score(df[target_col], classes)
        
        # Impressão das métricas
        print(f"Acurácia: {acuracia:.2f}")
        print(f"Precisão: {precision:.2f}")
        print(f"F1-Score: {f1:.2f}")
        print(f"Recall: {recall:.2f}")
        
        # Geração do relatório de classificação
        report = classification_report(df[target_col], classes, output_dict=True)
        report = pd.DataFrame(report)
        
        # Definição dos caminhos para salvar o relatório e o DataFrame resultante
        report_path, df_result_path = "./Docs/prod_report.parquet", "./Docs/previsoes.parquet"
        
        # Salvar o DataFrame resultante e o relatório de classificação
        df_result.to_parquet(df_result_path, index=False)
        report.to_parquet(report_path, index=False)

        # Fazer o log dos artefatos
        mlflow.log_artifact(df_result_path)
        mlflow.log_artifact(report_path)       

        # Fazer o log das métricas
        mlflow.log_metric("Acurácia",acuracia)
        mlflow.log_metric("Precisão",precision)
        mlflow.log_metric("F1-Score",f1)
        mlflow.log_metric("Recall",recall)
        
        # Impressão do relatório de classificação
        print(classification_report(df[target_col], classes))
        
    # Finalizar a execução da corrida
    mlflow.end_run()
    return report, acuracia, precision, f1, recall, df_result

def modelo_producao(df_producao, mlflow_db, model_uri, experiment_id, experiment_name):
    import os
    import pandas as pd
    import mlflow
    import Utils.functions as utils_func
   
    # Definição do URI do banco de dados MLflow
    os.environ['MLFLOW_TRACKING_URI'] = f"sqlite:///Code/Models/{mlflow_db}.db"

    # Configuração do ID do experimento
    experiment_id = utils_func.get_experiment_id(experiment_id)
    
    with mlflow.start_run(experiment_id=experiment_id, run_name=experiment_name) as run:
            
        # Carregamento do modelo treinado
        model = mlflow.sklearn.load_model(model_uri)
       
        # Previsão das classes no conjunto de dados de produção
        classes_producao = model.predict(df_producao)
        probabilities_producao = model.predict_proba(df_producao)[:, 1]  
       
        # Criação de um DataFrame com as previsões
        df_predicoes = pd.DataFrame({'Previsoes': classes_producao, 'Probabilidade_Classe_1': probabilities_producao})

        # Log dos artefatos
        df_predicoes_path= "./Docs/previsoes.parquet"
        df_predicoes.to_parquet(df_predicoes_path, index=False)
        mlflow.log_artifact(df_predicoes_path)
    
       
        # Adicionar as colunas originais ao DataFrame de previsões
        df_result = pd.concat([df_producao, df_predicoes], axis=1)
        
        # Impressão do DataFrame resultante
        print(df_result)
        
    # Finalizar a execução da corrida
    mlflow.end_run()
    return df_result
