def get_experiment_id(id_experimento):
    import mlflow
    # Verificar se o experimento existe, senão criar um novo
    experimento = mlflow.get_experiment_by_name(id_experimento)
    if experimento is None:
        id_experimento = mlflow.create_experiment(id_experimento)
        experimento = mlflow.get_experiment(id_experimento)
    id_experimento = experimento.experiment_id
    return id_experimento

def carregar_df(arquivo_carregado):
    import pandas as pd
    if arquivo_carregado is not None:
        if arquivo_carregado.name.endswith('.csv'):
            # Carregar o arquivo CSV
            df = pd.read_csv(arquivo_carregado)
        elif arquivo_carregado.name.endswith('.parquet'):
            # Carregar o arquivo Parquet
            df = pd.read_parquet(arquivo_carregado)
        else:
            st.error("Por favor, forneça um arquivo CSV ou Parquet, pois o formato do arquivo carregado não é suportado.")
            st.stop()
        return df
    else:
        st.error("Parece que nenhum arquivo foi fornecido. Por favor, carregue um arquivo para prosseguir com a análise.")
        st.stop() 

def formatar_run_uri(model_uri):
    try:
        model_uri_parts = model_uri.split("/")  
        model_uri_parts.remove('artifacts')
        model_uri_relative = "/".join(model_uri_parts[-2:])  
        formatted_model_uri = f"runs:/{model_uri_relative}"  
    except:
        formatted_model_uri = "runs:/"
    return formatted_model_uri

def formatar_modelo_uri():
    import streamlit as st
    try:
        model_name = st.sidebar.text_input("Nome do modelo:", value=st.session_state.experiment_name)
        alias = st.sidebar.text_input("Estado do Modelo(alias):", value="")
        formatted_model_uri = f"models:/{model_name}@{alias}"
    except:
        model_name, alias, formatted_model_uri = "", "", "models:/"