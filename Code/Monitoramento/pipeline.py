def monitoramento():
    import streamlit as st
    import pandas as pd
    import Utils.methods as metodos_utils
    import Utils.functions as func_utils
    
    st.sidebar.title("Monitoramento")
    
    # Escolha do dataset de produção para comparação (opcional)
    data_prod_choice = st.sidebar.toggle("(Opcional) Dataset para comparação:", value=True)
    if data_prod_choice is True:
        file_to_load = st.sidebar.file_uploader("Carregar Dataset:", type=["csv", "parquet"])
        if file_to_load is not None:
            dados_producao = func_utils.carregar_df(file_to_load)     
        else:
            dados_producao = False    
    else:
        dados_producao = False 
    
    try:
        columns = dados_producao.columns.tolist()
        target_col = st.sidebar.selectbox("Coluna alvo:", options=columns)
    except:
        columns = "" 
        target_col = st.sidebar.text_input("Coluna alvo:")

    try:
        target_col = st.session_state.target_col    
        mlflow_db = st.session_state.mlflow_db 
        model_uri = st.session_state.mlflow_model_uri 
        model_uri = func_utils.formatar_run_uri(model_uri)
    except:
        mlflow_db, model_uri = "", ""

    # Entrada do usuário para o banco de dados MLflow e URI do modelo
    mlflow_db = st.sidebar.text_input("BD rastreamento:", f"{mlflow_db}")
    model_uri = st.sidebar.text_input("Caminho do Modelo:", f"{model_uri}")

    try:
        if st.sidebar.button("Verificar"):
            # Comparação dos datasets e geração de relatórios
            relatorio_treino, relatorio_teste, relatorio_producao = comparar_datasets(mlflow_db, model_uri, target_col, dados_producao)
            
            st.write("Relatório de Treino")
            st.dataframe(relatorio_treino)
            st.write("Relatório de Teste")
            st.dataframe(relatorio_teste)
            
            if relatorio_producao is not None:
                st.write("Relatório de Classificação")
                st.dataframe(relatorio_producao)     
    except Exception as e:
        st.write(f"Verificar parâmetros.")

def comparar_datasets(mlflow_db, model_uri, target_col, dados_producao):
    from sklearn import metrics
    import mlflow
    import numpy as np
    import pandas as pd
    import Utils.plot as plot_utils
    
    # Configuração do URI de rastreamento do MLflow
    mlflow.set_tracking_uri(f"http://127.0.0.1:5000")
    #mlflow.set_tracking_uri(f"sqlite:///Code/Models/{mlflow_db}.db") 

    # Carregar modelo treinado
    model = mlflow.sklearn.load_model(model_uri)    

    # Download dos datasets de treino e teste
    train_run = mlflow.artifacts.download_artifacts(artifact_uri=f"{model_uri.split('pycaret-model')[0]}base_train.parquet")
    test_run = mlflow.artifacts.download_artifacts(artifact_uri = f"{model_uri.split('pycaret-model')[0]}base_test.parquet")
    dados_treino = pd.read_parquet(train_run)
    dados_teste = pd.read_parquet(test_run)
    
    # Previsões e probabilidades para os datasets de treino e teste
    predicoes_treino = model.predict(dados_treino.drop(target_col, axis=1))    
    predicoes_teste = model.predict(dados_teste.drop(target_col, axis=1))
    
    probabilidades_treino = model.predict_proba(dados_treino.drop(target_col, axis=1))
    probabilidades_teste = model.predict_proba(dados_teste.drop(target_col, axis=1))

    predicoes_classe_1_treino = probabilidades_treino[:, 1]
    predicoes_classe_1_teste = probabilidades_teste[:, 1]
    predicoes_classe_0_treino = probabilidades_treino[:, 0]
    predicoes_classe_0_teste = probabilidades_teste[:, 0]
    
    # Relatórios de classificação para os datasets de treino e teste
    relatorio_treino = metrics.classification_report(dados_treino[target_col], predicoes_treino, output_dict=True)
    relatorio_treino_df = pd.DataFrame(relatorio_treino)
    relatorio_teste = metrics.classification_report(dados_teste[target_col], predicoes_teste, output_dict=True)
    relatorio_teste_df = pd.DataFrame(relatorio_teste)
       
    if dados_producao is not False:
        # Limpeza do dataset de produção e previsões
        colunas_producao = dados_treino.columns.tolist()
        dados_producao_limpos = dados_producao[colunas_producao].dropna()
        
        predicoes_producao = model.predict(dados_producao_limpos.drop(target_col, axis=1))
        probabilidades_producao = model.predict_proba(dados_producao_limpos.drop(target_col, axis=1))
        predicoes_classe_1_producao = probabilidades_producao[:, 1]
        predicoes_classe_0_producao = probabilidades_producao[:, 0]
        
        # Relatório de classificação para o dataset de produção
        relatorio_producao = metrics.classification_report(dados_producao_limpos[target_col], predicoes_producao, output_dict=True) 
        relatorio_producao_df = pd.DataFrame(relatorio_producao)
        
        # Plotagem das previsões
        plot_utils.plot_predictions(predicoes_treino, predicoes_teste, predicoes_classe_1_treino, predicoes_classe_1_teste, predicoes_classe_0_treino, predicoes_classe_0_teste, predicoes_classe_0_producao, predicoes_classe_1_producao, predicoes_producao)

        return relatorio_treino_df, relatorio_teste_df, relatorio_producao_df
    else:
        # Plotagem das previsões quando não há dataset de produção
        plot_utils.plot_predictions(predicoes_treino, predicoes_teste, predicoes_classe_1_treino, predicoes_classe_1_teste, predicoes_classe_0_treino, predicoes_classe_0_teste, predicoes_classe_0_producao=False, predicoes_classe_1_producao=False, predicoes_producao = False)
    return relatorio_treino_df, relatorio_teste_df, None
