Instruções
======

Este projeto consiste em uma aplicação para treinamento, avaliação e aplicação de modelos de aprendizado de máquina usando MLflow e Streamlit. Abaixo estão as instruções para executar o projeto:

1\. Configuração do Ambiente
----------------------------

Certifique-se de ter o Python instalado em sua máquina. Recomenda-se o uso de um ambiente virtual para evitar conflitos de dependências.

    pip install virtualenv
    virtualenv venv
    source venv/bin/activate  # No Windows, use venv\Scripts\activate
    

Em seguida, instale as dependências do projeto:

    pip install -r Docs/requirements.txt
    

2\. Executando o MLflow UI
--------------------------

O MLflow Tracking permite visualizar e comparar experimentos e lançamentos (runs) usando o MLflow UI. Para iniciar o MLflow UI e apontá-lo para o banco de dados onde estão armazenados os resultados dos experimentos (nesse caso, SQLite), use o seguinte comando:

    mlflow ui --backend-store-uri sqlite:///Code/Models/mlruns.db
    

Este comando inicializa o servidor do MLflow UI e permite que você visualize os experimentos, modelos e métricas.

3\. Executando o Dashboard Streamlit
------------------------------------

O Streamlit é uma ferramenta de código aberto usada para criar aplicativos da web de maneira rápida e fácil a partir de scripts Python. Para iniciar o dashboard Streamlit, use o seguinte comando:

    streamlit run Code/dashboard.py
    

Este comando iniciará o servidor do Streamlit e abrirá automaticamente o dashboard no navegador da web padrão.

4\. Utilizando a Aplicação
--------------------------

*   No dashboard Streamlit, você poderá treinar modelos, avaliar modelos treinados, fazer previsões e monitorar o desempenho do modelo em produção.
*   No MLflow UI, você poderá visualizar todos os experimentos, modelos treinados e métricas associadas.

Certifique-se de ter os datasets necessários disponíveis e de seguir as instruções na interface da aplicação para carregá-los e executar as operações desejadas.